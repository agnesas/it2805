1. Why does inline style CSS override rules defined in style elements and external stylesheets?

Because it is more specific, inline CSS with the style=".." attribute always takes precedence.
You would need to use !important to overrule that.


2. Give a brief example of when to use ID (#id) and when to use classes (.class) in CSS.

The id selector is used to choose a single HTML element with a special value for the id property.
The class selector is used to select one or more HTML elements that have the same class attribute value.


3. What does RGBA mean and what colors can you express with it?

RGBA means the same as RGB, with an added alpha channel.
An RGBA color value consists of red, green, blue, and alpha channels. The syntax is rgba(red, green, blue, alpha), where the alpha
parameter is a number between 0.0 (fully transparent) and 1.0 (fully opaque).

The red, green, and blue parameters each have a value between 0 and 255 that describes the color's intensity.
This indicates that there are 16777216 possible colors, which is calculated like this: 256 * 256 * 256.
For instance, if one writes rgb(255, 0, 0), it displays red. This is because red is assigned to its greatest value (255), 
and the other two colors (green and blue) are set to 0.


4. Why do we include CSS files inside the head element and not inside the body element?

CSS belongs in the head as it isn't document content. 
Additionally, even if it works, every other Web developer would expect to see it there, 
so don't make things unclear by placing it in the body. Although one typically stay away from inline styles, 
inline CSS is the only CSS that should be used in the body.


5. What CSS selector matches all the p elements inside the article element in the following HTML?

The CSS selector that matches all the p elements inside the article element in the HTML is:
article > p

In CSS, the greater than sign (>) selector is used to select an element that has a specific parent. 
It's known as the element > element selector. It is also known as the child combinator selector because it only selects elements 
that are direct children of a parent. It only looks one level down the markup structure and not any deeper. 
Elements that are not the direct child of the specified parent are not chosen.